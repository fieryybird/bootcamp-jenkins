pipeline {
    agent any
    tools {
        maven 'maven-3.6.3'
    }
    stages {
        stage("increment version") {
            steps { 
                script {
                    sh 'echo "Incrementing app version..."'
                    sh 'mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                        versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def extract_vers = matcher[0][1]
                    env.VERSION = "$extract_vers"
                }
            }
        }
        stage("build jar file") {
            when {
                expression {
                    BRANCH_NAME == 'main'
                }
            }
            steps {
                sh 'mvn clean package'
            }
        }
        stage("build and push image to Nexus"){
            when {
                expression {
                    BRANCH_NAME == 'main'
                }
            }
            steps{
                withCredentials([usernamePassword(credentialsId: 'nexus-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                    sh 'echo Building Docker image for version $VERSION'
                    sh "docker build -t 37.187.124.95:8888/demojavapp:${VERSION} ."
                    sh "echo $PASS | docker login 37.187.124.95:8888 -u $USER --password-stdin"
                    sh "docker push 37.187.124.95:8888/demojavapp:${VERSION}"
                }
            }
        }
        stage("commit version update") {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config --global user.name "jenkins"'
                        sh 'git config --global user.email "jenkins@cicd.com"'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/fieryybird/bootcamp-jenkins.git"
                        sh 'git add .'
                        sh 'git commit -m "CI: version bump"'
                        sh 'git push origin HEAD:main'
                    }                    
                }
            }
        }
    }
    post {
        always {
            sh 'echo "Pipeline was successful. GJ! Cleaning up..."'
            sh "docker rmi 37.187.124.95:8888/demojavapp:${VERSION}"
            cleanWs()
        }
    }
}
